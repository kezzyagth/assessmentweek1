let mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/belajarmongo', { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('debug', true);
let Schema = mongoose.Schema;

const taskSchema = new Schema(
    {
        id:Number,
        title:String,
        description:String,
        dueDate:Date,
        comments:[String],
        status:{
            type:String,
            enum:[`new`,`in-progress`,`complete`]
    }}
);

const Task = mongoose.model('task', taskSchema);


module.exports = {Task, taskSchema}
