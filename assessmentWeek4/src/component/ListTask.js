import React, {Component} from 'react';
import {Text, FlatList, View, TouchableOpacity} from 'react-native';
import axios from 'axios';
import {NavigationEvents} from 'react-navigation';
import {connect} from 'react-redux';
import style from '../style/styleSheetCss'
import getTaskList from '../action'

class listTasks extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
          isLoading: true,
          tasks: [],
        };
      }

    componentDidMount() {
        axios.get('http://192.168.43.159:3000/task', {
            auth : {
                username: 'john',
                password: 'secret',
            },
        })
        .then(res =>  {
            const tasks = res.data;
            this.setState({
                tasks,
                isLoading: false,
            });
        });
    }

    renderItem = task => {
        return (
            <View style={style.list}>
                <TouchableOpacity
                onPress ={() => 
                this.props.navigation.navigate('DetailTask', task.item )}
                >
                    
                    <Text style={style.nameText}>{task.item.title} </Text>
                    <Text style={style.text}>Id: {task.item.id}  </Text>
                <Text style={style.text}>Description: {task.item.description} </Text>
                    <Text style={style.text}>dueDate: {task.item.dueDate} </Text>
                    <Text style={style.text}>Comments: {task.item.comment} </Text>
                    <Text style={style.text}>Status: {task.item.status} </Text>
                </TouchableOpacity>
            </View>
        );
    };

    render(){
        return (
            <View style={style.container}>
                <View style={style.header}>
                    <Text style={style.txtHeader}>List Tasks</Text>
                </View>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('DetailTask')}
                />
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderItem}
                    data={this.state.tasks}
                />
            </View>
        );
    }
}

const mapStateToProps= state => {
    return{
        task: state.task
    }
}

export default connect (mapStateToProps, {getTaskList}) (listTasks);