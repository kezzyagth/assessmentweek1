import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import style from '../style/styleSheetCss';
import axios from 'axios';
export default class detailTask extends Component {
  static navigationOptions = {
    title: `Tasks Details`,
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      task: '',
    };
  }
  componentDidMount() {
    const {getParam} = this.props.navigation;
    this.setState({
      task: {
        id: getParam('id'),
        title: getParam('title'),
        description: getParam('description'),
        dueDate: getParam('dueDate'),
        comments: getParam('comments'),
        status: getParam('status'),
      },
      updateTitle: false,
      updateDescription: false,
      updateDueDate: false,
      updateComments: false,
    });
  }
  updateTitle() {
    if (this.state.updateTitle) {
        return (
            <TextInput
              keyboardType="default"
              value={this.state.task.title.toString()}
              onChangeText={text =>
                this.setState({task: {...this.state.task, title: text}})
              }
            />
          );
    }
    return;
  }
  updateDescription() {
    if (this.state.updateDescription) {
      return (
        <TextInput
          keyboardType="default"
          value={this.state.task.description.toString()}
          onChangeText={text =>
            this.setState({task: {...this.state.task, description: text}})
          }
        />
      );
      return;
    }
  }

  updateDueDate() {
    if (this.state.updateDueDate) {
      return (
        <DatePicker
          style={style.datePicker, style.dateIcon,style.dateInput}
          date={this.state.task.dueDate}
          mode="date"
          format="YYYY-MM-DD"
          minDate={new Date().toISOString()}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={date => {
            this.setState({task: {...this.state.task, dueDate: date}});
          }}
        />
      );
    }
    return;
  }

  updateComments(item, index) {
    if (this.state[`updateComments${index}`]) {
      let temp = this.state.task.comments;
      return (
        <TextInput
          value={this.state.task.comments[index]}
          onChangeText={text => {
            temp[index] = text;
            this.setState({task: {...this.state.task, comments: temp}});
          }}
          onEndEditing={e => {
            this.setState({
              [`updateComments${index}`]: !this.state[
                `updateComments${index}`
              ],
            });
          }}
        />
      );
    }
    return (
      <Text
        onPress={() =>
          this.setState({
            [`updateComments${index}`]: !this.state[
              `updateComments${index}`
            ],
          })
        }>
        {item}
      </Text>
    );
  }
  addComments() {
    if (this.state.addComments) {
      let temp = this.state.task.comments;
      return (
        <TextInput
          style={{paddingLeft: 10}}
          placeholder="Input the new comments"
          onEndEditing={e => {
            temp.push(e.nativeEvent.text);
            this.setState({
              addComments: !this.state.addComments,
              task: {...this.state.task, comments: temp},
            });
          }}
        />
      );
    }
    return;
  }
  render() {
    return (
      <View style={style.container}>
          <View style={style.data}>
              <Text 
              style = {style.details}
              onPress={() =>
                this.setState({
                  updateTitle: !this.state.updateTitle,
                })
              }>
              Title : {this.state.task.title}
              </Text>
              <Text
              style = {style.details}
              onPress={() => this.setState({updateDescription: !this.state.updateDescription})}>
              Description : {this.state.task.description}
              </Text>
              <Text 
              style = {style.details}
              onPress={() =>
                this.setState({
                  updateDueDate: !this.state.updateDueDate,
                })
              }>
              dueDate : {this.state.task.dueDate}
            </Text>
            <Text
            style = {style.details}
              onPress={() =>
                this.setState({addComments: !this.state.addComments})
              }>
              Comments :
            </Text>
            {this.addComments()}
            <FlatList
            style = {style.details}
              style={{paddingLeft: 30}}
              data={this.state.task.comments}
              keyExtractor={({index}) => index}
              renderItem={({item, index}) =>
                this.updateComments(item, index)
              }
            />
          </View>
      </View>
    );
  }
  async componentWillUnmount() {
    await axios.put(
      `http://192.168.43.159:3000/task/${this.state.task.id}`,
      this.state.task,
      {
        auth: {
          username: 'john',
          password:
            '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm', // 'secret'
        },
      },
    );
  }
}