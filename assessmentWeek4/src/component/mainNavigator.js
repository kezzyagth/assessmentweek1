import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import DetailTask from './DetailTask';
import ListTask from './ListTask';

const MainNavigator = createStackNavigator(
  {
    ListTask: {screen: ListTask},
    DetailTask: {screen: DetailTask},
  },
  {
    initialRouteName: 'ListTask',
  },
);

const Container = createAppContainer(MainNavigator);

export default Container;
