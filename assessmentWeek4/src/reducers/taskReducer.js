const initialState={
    loading: false,
    tasks:[],
    error: null
}

const taskReducer=(state=initialState, action) => {
    switch (action.type){
        case 'GET_TASK_LIST_STARTED':
            return{
                ...state,
                loading: true
            }
            case 'GET_TASK_LIST_SUCCESS':
                return {
                    ...state,
                    error: null,
                    loading: false,
                    tasks: action.payload
                }
                case 'GET_TASK_LIST_FAILURE' :
                    return {
                        ...state,
                        loading: false,
                        error: action.payload.error
                    }
                default :
                 return state
    }
}

export {taskReducer}