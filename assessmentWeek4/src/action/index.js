import axios from 'axios';

const getTaskListStarted = () => {
    return{
        type: 'GET_TASK_LIST_STARTED',       
    }
}

const getTaskListFailure = error => {
    return{
        type: 'GET_TASK_LIST_FAILURE',
        payload: {
            error
        }
    }
}

const getTaskListSuccess = pets => {
    return{
        type: 'GET_TASK_LIST_SUCCESS',
        payload: {
            tasks
        }
    }
}

export const getTaskList = () => {
    return dispatch => {
        dispatch(getTaskListStarted())
        axios.get(`http://192.168.43.159:3000/task`, {
            auth: {
                username: 'john',
                password: 'secret'
            }
        })
        .then(res => {
            dispatch(getTaskListSuccess(res.data))
        }).catch(err => {
            dispatch(getTaskListFailure(err))
        })

    }

}