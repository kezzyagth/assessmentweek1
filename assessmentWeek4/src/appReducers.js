import React,{Component} from 'react'
import store from './store'
import MainNavigator from './component/mainNavigator'
import {Provider} from 'react-redux'

export default class App extends Component {
    render(){
        return(
            <Provider store={store}>
                <MainNavigator/>
            </Provider>
        )
    }
}