# Assessments Week 4

## What is the approach?
Created a React Native UI for The Personal Task Tracker

## Why is the approach followed?
Because I just understand it that way.

## Problems faced while using the approach
* 1. Thinking about UI design
* 2. Connect the UI to the database
* 3. How to make them works

## Steps to run the code
```
git clone git@gitlab.com:kezzyagth/assessmentweek4.git
configurate the IP
connect your phone to the pc or use emulator
run yarn init in terminal
run react-native run-android in terminal
```
