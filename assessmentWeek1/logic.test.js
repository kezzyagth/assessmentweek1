const logic = require('./logic')

const cubeColor = [['Yellow', 'Yellow', 'Red'], ['Yellow', 'Red'], ['Red'], ['Red'], ['Yellow', 'Red'], ['Yellow', 'Yellow', 'Red']]

test('check if replace to one cube equals to', () => {
    expect(logic.replaceToOne(cubeColor, 'Yellow', 'Red')).toEqual([['Red', 'Red', 'Red'], ['Red', 'Red'], ['Red'], ['Red'], ['Red', 'Red'], ['Red', 'Red', 'Red']])
})

test('check if replace to many cube equals to', ()=>{
    expect(logic.replaceToMany(cubeColor, 'Yellow', 'Yellow', 'Red')).toEqual([['Yellow', 'Red', 'Yellow', 'Red', 'Red'], ['Yellow', 'Red', 'Red'], ['Red'], ['Red'], ['Yellow', 'Red', 'Red'], ['Yellow', 'Red', 'Yellow', 'Red', 'Red']])
})

test('check if reject cube equals to', ()=>{
    expect(logic.reject(cubeColor, 'Yellow')).toEqual([['Red'], ['Red'], ['Red'], ['Red'], ['Red'], ['Red']])
})

test('check if stack cubes equals to', ()=>{
    expect(logic.stack(cubeColor, 'Yellow')).toEqual([['Yellow', 'Yellow', 'Red', 'Yellow'], ['Yellow', 'Red', 'Yellow'], ['Red', 'Yellow'], ['Red', 'Yellow'], ['Yellow', 'Red', 'Yellow'], ['Yellow', 'Yellow', 'Red', 'Yellow']])
})

test('check if reverse cube equals to', ()=>{
    expect(logic.reverse(cubeColor)).toEqual([['Red', 'Yellow', 'Yellow'], ['Red' ,'Yellow'], ['Red'], ['Red'], ['Red', 'Yellow'], ['Red', 'Yellow', 'Yellow']])
})



