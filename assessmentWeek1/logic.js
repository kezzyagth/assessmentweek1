const replaceToOne=(arr,initColor,finalColor)=>{
    return arr.map(cube => {
        return cube.map(color=>{
            if(color==initColor){
                return finalColor
            } else{
                return color
            }
        })
    })
}

const replaceToMany = (arr, initialColor, finalColor1,finalColor2) => {
    return arr.map(cubeInit => { {
        let Cubes = []
        cubeInit.forEach(color => { 
            if(color==initialColor){
                Cubes.push(finalColor1),
                Cubes.push(finalColor2)
            }else{
                Cubes.push(color)
            }
        })
        return Cubes
    }})
}

const reject = (arr, finalColor) => {
    return arr.map(cube => {
        let Cubes = []
        cube.forEach((color) => {
            if(color!=finalColor){
                Cubes.push(color)
            }
        })
        return Cubes
    }).filter(cube => cube.length)
}


const stack = (arr, finalColor) => {
    return arr.map(cube =>{
        let Cubes = []
        cube.forEach((color, index, array) => {
            Cubes.push(color)
            if(index == array.length-1){
                Cubes.push(finalColor)
            }
        })
        return Cubes
    })
}

const reverse = (arr) => {
    return arr.map(cube => cube.reverse())
}





module.exports = { replaceToOne , replaceToMany ,reject ,stack ,reverse}
