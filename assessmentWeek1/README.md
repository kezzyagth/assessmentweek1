# Assessments Week 1

## What is the approach?
First, I made the function (logic.js) and then test the function (logic.test.js). Second, I made the solutions for the level (levelSolutions.js) and made level.js.

## Why is the approach followed?
Because I just understand that way.

## Problems faced while using the approach
* 1. The first problem is figured how the game works, and how to solve all the levels.
* 2. The second is what kind of approach I will use
* 3. How to make the game works

## Steps to run the code
`git clone https://gitlab.com/kezzyagth/assessmentweek1.git`
`run node level.js`
`run jest for the .test.js file`

