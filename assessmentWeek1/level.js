const level = require('./levelSolutions')
const Yellow='Yellow'
const Red='Red'

const initial= [[Yellow,Yellow,Red],[Yellow,Red],[Red],[Red],[Yellow,Red],[Yellow,Yellow,Red]]
const final1= [[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red]]
const final2= [[Red],[Red],[Red],[Red],[Red],[Red]]
const final3= [[Red,Yellow],[Red,Yellow],[Red,Yellow],[Red,Yellow],[Red,Yellow],[Red,Yellow]]
const final4= [[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red],[Red,Yellow,Red]]

const level1=level.levelSolutions(initial, final1)
const level2=level.levelSolutions(initial, final2)
const level3=level.levelSolutions(initial, final3)
const level4=level.levelSolutions(initial, final4)

level1.getStep().forEach(element => {
  console.log(element)  
})
