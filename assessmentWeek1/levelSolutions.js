let logic = require('./logic')

const levelSolutions = (initArray, finalArray)=>{
    initial = initArray
    final = finalArray
    current = initial 
    step = [] 
    let possible_moves = [
        [1],[2],[3],[4],
        [1,2],[1,3],[1,4],[2,1],[2,3],[2,4],[3,1],[3,2],[3,4],[4,1],[4,2],[4,3],
        [1,2,3],[1,2,4],[1,3,2],[1,3,4],[1,4,2],[1,4,3],
        [2,1,3],[2,1,4],[2,3,1],[2,3,4],[2,4,1],[2,4,3],
        [3,1,2],[3,1,4],[3,2,1],[3,2,4],[3,4,1],[3,4,2],
        [4,1,2],[4,1,3],[4,2,1],[4,2,3],[4,3,1],[4,3,2],
        [1,2,3,4],[1,2,4,3],[1,3,2,4],[1,3,4,2],[1,4,2,3],[1,4,3,2],
        [2,1,3,4],[2,1,4,3],[2,3,1,4],[2,3,4,1],[2,4,1,3],[2,4,3,1],
        [3,1,2,4],[3,1,4,2],[3,2,1,4],[3,2,4,1],[3,4,1,2],[3,4,2,1],
        [4,1,2,3],[4,1,3,2],[4,2,1,3],[4,2,3,1],[4,3,1,2],[4,3,2,1]
    ]
    let count = 0
    while(final.toString() != current.toString()){
        current = initial
        step = []
        step.push(current)    
        if(count==63){
            break 
        } else {
            let step_count = 0
            possible_moves[count].forEach(element=>{
                if(element==1){ 
                    current=logic.replaceToOne(current, 'Yellow', 'Red')
                    step_count++
                    step.push(`Step ${step_count}: map {Yellow} -> {Red}`)
                    step.push(current)
                } else if(element==2){
                    current=logic.stack(current,'Yellow')
                    step_count++
                    step.push(`Step ${step_count}: map(stack {Yellow})`)
                    step.push(current)
                } else if(element==3){
                    current=logic.replaceToMany(current, 'Yellow', 'Yellow', 'Red')
                    step_count++
                    step.push(`Step ${step_count}: map {Yellow} -> {Red}, {Yellow}`)
                    step.push(current)
                } else if(element==4){
                    current=logic.reject(current, 'Yellow')
                    step_count++
                    step.push(`Step ${step_count}: map(reject {Yellow})`)
                    step.push(current)
                }
            })
        }
        count++ 
    }
    const getStep=()=>{
        return step
    }
    return {getStep}
}
module.exports = {levelSolutions}
