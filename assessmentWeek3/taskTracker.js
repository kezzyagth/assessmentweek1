const {Task, taskSchema} = require('./task');

const getAllTask = function (request, h) {
    request.log('error', 'Event error');
    logging(request, h)
    return Task.find()
}

const getTask = async (filter) => {
    return await PerformanceTiming.findOne({id:filter}).lean()
}

const getTaskById = function (request,h) {
    return Task.findOne({id: request.params.id})
};

const getTaskByQuery= async function (request, h) {
    const query = request.query
    const blank = JSON.stringify(query)==`{}`
    if(blank){
        const result= await Task.find({status:{$in:[`new`,`in-progress`]}},{_id:0,__v:0})
                                .sort({dueDate:1,title:1})
                                .lean()
        return h.response(result).code(200)
    } else {
        let sortBy=query.sort?query.sort:`dueDate`
        let offset=query.offset?parseInt(query.offset):0
        let limit=query.limit?parseInt(query.limit):10
        let filter=query.filter?query.filter:{status:{$in:[`new`,`in-progress`]}}
        if(!fields.some(field=>field==sortBy)){
            return h.response(error.message).code(400)
        }
        if(query.dueDate){
            Object.assign(filter,{dueDate:new Date(query.dueDate)})
        }
        const result=await Task.find(filter,{_id:0,__v:0},)
            .sort({[sortBy]:1,dueDate:1,title:1})
            .skip(offset)
            .limit(limit)
            .lean()
        return h.response(result).code(200)
    }
};

const postTask = async function (request, h) {
    try {
        let max = await Task.aggregate([{ $group: { _id: "id", maxId: { $max: "$id" } } }])
        const newId = max[0].maxId + 1
        Object.assign(request.payload,{id: newId, status: `new`})
        let tasking = new Task(request.payload);
        let result = await tasking.save();
        return h.response(result);
    } catch (error) {
        return h.response(error.message).code(500);
    }
};

const updateTask = async function (request,h){
    try {
        let result = await Task.findOneAndUpdate(request.params.id, request.payload, {new: true});
        return h.response(result);
    }catch (error) {
        return h.response(error).code(500);
    }
}

const deleteTask = async function (request, h) {
    try {
        let result = await Task.findOneAndUpdate({id:request.params.id,status:{$in:[`new`,`in-progress`]}},{status:`complete`}).lean()
        return h.response(result);
    }catch (error){
        return h.response(error).code(500)
    }
};

const logging = async (request, h) =>{
    request.log(['a', 'b'], 'Request into Task')
 
    request.logger.info('In handler %s', request.path)

    return 'Hello!'
}


module.exports = {getAllTask, getTask, getTaskById, getTaskByQuery, postTask, updateTask, deleteTask};