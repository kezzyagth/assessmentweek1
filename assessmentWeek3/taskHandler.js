const Path = require('path');
const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi')
const Bcrypt = require('bcryptjs');
const CatboxRedis = require('@hapi/catbox-redis');
const { getTask, getAllTask, getTaskById, getTaskByQuery, updateTask, postTask, deleteTask} = require('./taskTracker')

const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const server = Hapi.server({
    port: 3000,
    host: '0.0.0.0',
    cache:[
            {
                name: 'my_cache',
                provider: {
                    constructor: CatboxRedis,
                    options: {
                        partition : 'my_cached_data',
                        host: 'localhost',
                        port: 6379,
                        database: 0
                    }
                }
            }
        ]
});

exports.init = async () => {

    await server.initialize();
    return server;
};

exports.start = async () => {
    await server.register(require('@hapi/basic'));
    await server.register({
        plugin: require('hapi-pino'),
        options: {
          prettyPrint: process.env.NODE_ENV !== 'production',
          stream:'./logs.log',
          redact: ['req.headers.authorization']
        }
    })

    server.auth.strategy('simple', 'basic', { validate });

    server.route({
        method: 'GET',
        path: '/task',
        handler: getAllTask, 
        options: {
            auth: 'simple',
            log:{
                collect: true
            }
        }
    })

    server.route({
        method: 'GET',
        path: '/task/{id}',
        options: {
            validate: {
                params: {
                    id: Joi.number().integer().min(1)
                }
            },
            auth: 'simple'
        },
        handler: getTaskById,
    })

       server.route({
        method: 'GET',
        path: '/task/query',
        options:{
            auth: 'simple',
            validate: {
                query: {
                    sort:Joi.string(),
                    offset:Joi.number().integer().min(0),
                    limit:Joi.number().integer().min(1),
                    filter:Joi.object(),
                    dueDate:Joi.date()
                }
            }
        },
        handler: getTaskByQuery
    })

    server.route({
        method: "POST",
        path: "/task",
        options: {
            validate: {
                payload: {
                    id:Joi.any().forbidden(),
                    title:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/).required(),
                    description:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/).required(),
                    dueDate:Joi.date().min(Date.now()).required(),
                    comments:Joi.array().required(),
                    status:Joi.any().forbidden()
                }
            },
            auth: 'simple'
        },
        handler: postTask
    });

    server.route({
        method: 'PUT',
        path: '/task/{id}',
        options: {
            validate: {
                params: {
                    id:   Joi.number().integer(),
                    payload: {
                        id:Joi.number().integer(),
                        title:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/),
                        description:Joi.string().regex(/^[a-zA-Z]+\s*[a-zA-Z]+$/),
                        dueDate:Joi.date().min(Date.now()),
                        comments:Joi.array(),
                        status:Joi.string().valid(`new`,`in-progress`,`complete`)
                    }
                }
            },
            auth: 'simple'
        },
        handler: updateTask,
    });

    server.route({
        method: 'DELETE',
        path: '/task/{id}',
        options: {
            validate: {
                params: {
                    id: Joi.required(),
                }
            },
            auth: 'simple'
        },
        handler: deleteTask,

    });

    server.method('getTask',getTask,{
        cache: {
            cache: 'my_cache',
            segment: 'task',
            expiresIn: 1000 * 1000,
            generateTimeout: 2000
        }
    })

    server.logger().info('Logging.log')

    server.log(['subsystem'], 'third way for accessing it')

    await server.start();
    console.log('Server running at:', server.info.uri);
    return server;
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});
